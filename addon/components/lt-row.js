import Ember from 'ember';
import layout from '../templates/components/lt-row';

const {
  computed,
  isEmpty,
  defineProperty
} = Ember;

export default Ember.Component.extend({
  layout,
  tagName: 'tr',
  // classNames: ['lt-row'],
  classNameBindings: ['isSelected', 'isExpanded', 'canExpand:is-expandable', 'canSelect:is-selectable','styleClass'],


  init() {
    this._super(...arguments);
    let cssStyleF=this.get('rowStyleF');
    if(cssStyleF && typeof cssStyleF.f === 'function'){
      let args=cssStyleF.valuePaths.map((valuePath)=>`row.${valuePath}`);
      args.push(()=>{
        return cssStyleF.f.call(this,this.get('row'));
      });
      defineProperty(this, 'styleClass', computed.apply(this,args).readOnly());
    }


  },

  attributeBindings: ['colspan'],

  rowStyleF:null,
  columns: null,
  row: null,
  tableActions: null,
  canExpand: false,
  canSelect: false,
  colpan: 1,

  isSelected: computed.readOnly('row.selected'),
  isExpanded: computed.readOnly('row.expanded')
});
